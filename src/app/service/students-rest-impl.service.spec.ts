import { TestBed } from '@angular/core/testing';

import { StudentsRestImplService } from './students-rest-impl.service';

describe('StudentsRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsRestImplService = TestBed.get(StudentsRestImplService);
    expect(service).toBeTruthy();
  });
});
